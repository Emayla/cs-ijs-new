<?php
function makeCollapsibleDiv($title, $section, $first=false) {
	if ($first) echo '<div id="' . $section . '" class="collapse in">';
	else echo '<div id="' . $section . '" class="collapse">';
	echo "\n";
	switch ($section) {
		case 'conferences':
			include('conferences.php');
			break;
		case 'academia':
		case 'industry':
			$today = getdate();
			$meja = $today['year'] - 5;
			$section = mysql_real_escape_string($section);
			$result = mysql_query("SELECT * FROM sodelovanje WHERE (tip='$section' AND leto_zadnje>$meja AND prikaz='1') ORDER BY ustanova ASC");
			echo "<ul> \n";
			while($row = mysql_fetch_array($result)) {
  			echo "<li>";
  			echo "$row[ustanova], $row[mesto], $row[drzava]";
  			echo "</li> \n";
  		}
			echo "</ul> \n";
			break;
		default:
			$filename = "researchareas/" . $section . ".php";
			if (is_file($filename)) include($filename);
			else echo "File not found.";
			break;
	}
	echo '</div>';
}

function printLinksAndDivs($links) {
	$first = true;
	foreach ($links as $key => $value) {
		echo "<div class='activity'> \n";
		echo "<div class='heading'> \n";
		echo "<a href='#". $key ."' data-toggle='collapse'><div class='toggle'> \n";
		if ($first) echo "<span class='glyphicon glyphicon-minus' aria-hidden='true'></span> \n";
		else echo "<span class='glyphicon glyphicon-plus' aria-hidden='true'></span> \n";
		echo "<h2>" . $value . "</h2> \n";
		echo "</div></a> \n";
		echo "</div> \n";
		makeCollapsibleDiv($value, $key, $first);
		echo "</div> \n\n";

		$first = false;
	}
}


?>

<script src="js/toggle.js"></script>
<main>
	<div class="container">
	<div class="wrapper">
	<div class="sidebar">
		<h1>Activities</h1>
		<ul>
			<li class='<?php echo ($detail == "" || $detail == "research" ? "active" : "") ?>'><a href="?show=activities&detail=research">Research areas</a></li>
			<li class='<?php echo ($detail == "collaboration" ? "active" : "") ?>'><a href="?show=activities&detail=collaboration">Collaboration</a></li>
			<li class='<?php echo ($detail == "organizing" ? "active" : "") ?>'><a href="?show=activities&detail=organizing">Event organizing</a></li>
		</ul>
	</div>

	<div class="content">
		<?php
			include('config.php');
			switch ($detail) {
				case 'collaboration':
					echo "<h1>Collaboration</h1>";
					$collaboration = array(
						'industry' => 'Industry', 
						'academia' => 'Academic'
						);
					printLinksAndDivs($collaboration);
					break;
				case 'organizing':
					echo "<h1>Event organizing</h1>";
					$organizing = array(
						'conferences' => 'Conferences'
						);
					printLinksAndDivs($organizing);
					break;
				case 'research':
				case '':
					echo "<h1>Research areas</h1>";
					$researchAreas = array(
						'advsyst' 	=> 'Advanced systems',
						'applcomp' 	=> 'Applied computing',
						'ehealth' 	=> 'eHealth'
						);
					printLinksAndDivs($researchAreas);
						break;
					default:
						include('404.php');
						break;
			}
			mysql_close($con);
		?>

	</div>
	</div>
	</div>
</main>