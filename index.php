<?php
  $naslov = "Computer Systems @ JSI";
  $potDoKorena = "";
  $potDoNavigacije = "";
  $prirejenaNoga = "";
  $robots = "";
  $description="The Computer Systems department at Jožef Stefan Institute is concerned primarily with the design automation of computing structures and systems. Within this broad area, we are concentrating particularly on metaheuristic optimization approaches to engineering design and logistics problems as well as electronic system design and test.";
  $keywords="high-level synthesis, HLS, test, diagnosis, sequential, built-in self-test, BIST, design for testability, DFT, parallel, real-time, optimization";

  $show = '';
  if (isset($_GET['show'])) {
    $show = $_GET['show'];
  }
  if ($show == 'home' || $show == '') $firstpage = true;
  else $firstpage = false;
  
  if ($potDoLokalnegaKorena='') $potDoLokalnegaKorena = $potDoKorena;

  $detail = '';
  if (isset($_GET['detail'])) {
    $detail = $_GET['detail'];
  }
  $id = '';
  if (isset($_GET['id'])) {
		$id = $_GET['id'];
	}
?>

<!DOCTYPE html>
<html lang="en">
<?php require('_header.php'); ?>

<body>
<?php 
  include('header.php'); 
  include ($potDoNavigacije . '_navigation.php');

  if (!empty($show)) {
    $filename = $show . '.php';
    if (is_file($filename)) include($filename);
    else include('404.php');
  }
  else include('home.php');

  include('_footer.php');
?>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>