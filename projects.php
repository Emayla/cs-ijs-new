<?php
	require('print_functions.php');
?>

<script src="js/toggle.js"></script>
<main>
	<div class="container">
	<div class="wrapper">
	<div class="sidebar">
		<h1>Projects</h1>
		<ul>
			<li class='<?php echo (($detail == "" && $id == "") || $detail == "research" ? "active" : "") ?>'><a href="?show=projects&detail=research">Research</a></li>
			<li class='<?php echo ($detail == "applied" ? "active" : "") ?>'><a href="?show=projects&detail=applied">Applied</a></li>
		</ul>
	</div>

	<div class="content">
		<?php
			if ($id != '') {
				printID($show, $id);
			}
			else {
				switch ($detail) {
					case 'research':
					case '':
						echo "<h1>Research projects<small><a href='?show=projects&detail=research";
						if (isset($_GET[$detail])) {
							if ($_GET[$detail] == 'all') echo "'>show recent";
							else echo "&research=all'>show all";
						}
						else echo "&research=all'>show all";
						echo "</a></small></h1> \n";
						printProjects('research');
						break;
					case 'applied':
						echo "<h1>Applied projects<small><a href='?show=projects&detail=applied";
						if (isset($_GET[$detail])) {
							if ($_GET[$detail] == 'all') echo "'>show recent";
							else echo "&applied=all'>show all";
						}
						else echo "&applied=all'>show all";
						echo "</a></small></h1> \n";
						printProjects($detail);
						break;
					default:
						include('404.php');
						break;
				}
			}	
		?>
	</div>
	</div>
	</div>
</main>