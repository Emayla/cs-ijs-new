﻿<?php
include('../config.php');

$today = getdate();
if ($_GET[journals] == 'all') {
    $journals = 1;
    $mejaj = 0;
} else {
    $journals = 0;
    $mejaj = $today['year'] - 3;
}
if ($_GET[conferences] == 'all') {
    $conferences = 1;
    $mejac = 0;
} else {
    $conference = 0;
    $mejac = $today['year'] - 3;
}
if ($_GET[books] == 'all') {
    $books = 1;
    $mejab = 0;
} else {
    $books = 0;
    $mejab = $today['year'] - 3;
}
if ($_GET[misc] == 'all') {
    $misc = 1;
    $mejam = 0;
} else {
    $misc = 0;
    $mejam = $today['year'] - 7;
}

for ($i = 1; $i < 5; $i++) {
    if ($i == 1)
        $result = mysql_query("SELECT * FROM publikacije INNER JOIN ((SELECT avtor_publikacija.pubID FROM avtor_publikacija WHERE (avtor_publikacija.avtorID = $osebaID AND prikaz='1')) AS DerivedTable) ON (publikacije.tip='journal' AND publikacije.leto>$mejaj AND publikacije.pubID = DerivedTable.pubID) ORDER BY leto DESC, publikacije.pubID DESC");
    else if ($i == 2)
        $result = mysql_query("SELECT * FROM publikacije INNER JOIN ((SELECT avtor_publikacija.pubID FROM avtor_publikacija WHERE (avtor_publikacija.avtorID = $osebaID AND prikaz='1')) AS DerivedTable) ON (publikacije.tip='conference' AND publikacije.leto>$mejac AND publikacije.pubID = DerivedTable.pubID) ORDER BY leto DESC, publikacije.pubID DESC");
    else if ($i == 3)
        $result = mysql_query("SELECT * FROM publikacije INNER JOIN ((SELECT avtor_publikacija.pubID FROM avtor_publikacija WHERE (avtor_publikacija.avtorID = $osebaID AND prikaz='1')) AS DerivedTable) ON (publikacije.tip='book' AND publikacije.leto>$mejab AND publikacije.pubID = DerivedTable.pubID) ORDER BY leto DESC, publikacije.pubID DESC");
    else if ($i == 4)
        $result = mysql_query("SELECT * FROM publikacije INNER JOIN ((SELECT avtor_publikacija.pubID FROM avtor_publikacija WHERE (avtor_publikacija.avtorID = $osebaID AND prikaz='1')) AS DerivedTable) ON (publikacije.tip='misc' AND publikacije.leto>$mejam AND publikacije.pubID = DerivedTable.pubID) ORDER BY leto DESC, publikacije.pubID DESC");


    if ($i == 1)
        echo "<h3>Journal articles";
    else if ($i == 2)
        echo "<h3>Conference papers";
    else if ($i == 3)
        echo "<h3>Books & Chapters";
    else if ($i == 4)
        echo "<h3>Other publications";

    echo"<span align=right><a href='?show=publications" . ($_GET['id'] != '' ? "&id=" . $_GET['id'] : "");

    if ($i == 1)
        echo (!$journals ? "&journals=all" : "") . ($conferences ? "&conferences=all" : "") . ($books ? "&books=all" : "") . ($misc ? "&misc=all" : "") . "'>" . ($journals ? "show recent" : "show all") . "</a>";
    else if ($i == 2)
        echo ($journals ? "&journals=all" : "") . (!$conferences ? "&conferences=all" : "") . ($books ? "&books=all" : "") . ($misc ? "&misc=all" : "") . "'>" . ($conferences ? "show recent" : "show all") . "</a>";
    else if ($i == 3)
        echo ($journals ? "&journals=all" : "") . ($conferences ? "&conferences=all" : "") . (!$books ? "&books=all" : "") . ($misc ? "&misc=all" : "") . "'>" . ($books ? "show recent" : "show all") . "</a>";
    else if ($i == 4)
        echo ($journals ? "&journals=all" : "") . ($conferences ? "&conferences=all" : "") . ($books ? "&books=all" : "") . (!$misc ? "&misc=all" : "") . "'>" . ($misc ? "show recent" : "show all") . "</a>";

    echo"</span></h3>\n";

    if (mysql_num_rows($result) > 0) {
        echo "<span class='readmore'>";

        while ($row = mysql_fetch_array($result)) {
            echo "<a href='?show=publications&id=" . $row[pubID] . ($journals ? "&journals=all" : "") . ($conferences ? "&conferences=all" : "") . ($books ? "&books=all" : "") . ($misc ? "&books=all" : "") . "'>";
            if ($_GET['id'] == $row[pubID])
                echo "<span id='obarvaj'>" . $row[naslov] . "</span>";
            else
                echo $row[naslov];
            echo "</a>";

            if ($i == 1)
                echo ",<i> $row[akronim]</i> ($row[leto])";
            else if ($i == 2)
                echo ", $row[akronim]";
            else if ($i == 3)
                echo ($row[knjiga] != '' ? ", <i>" . $row[knjiga] . "</i>" : "") . ", $row[zalozba] ($row[leto])";
            else if ($i == 4)
                echo " ($row[leto])";
            echo "<br>\n";
        }

        echo "</span>\n";
    }

    echo "<p>&nbsp;</p>";
}

mysql_close($con);
?>