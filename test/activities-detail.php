<?php
switch ($_GET['detail']) {
    case 'mentorship':
        ?>
		    <h2>Mentorship</h2>
        <h3>Doctoral dissertations</h3>
       	<blockquote>
       	</blockquote>
        <h3>Master's theses</h3>
       	<blockquote>
       	</blockquote>
        <h3>Undergraduate theses</h3>
       	<blockquote>
            <li>SOMEONE, <a href="http://www.fri.uni-lj.si/" target="_blank"> FRI</a>, (in progress)</li>
       	</blockquote>
        <?php
        break;
    case 'reviewer':
        ?>
				<h2>Reviewer</h2>
       	<h3>Journals</h3>
       	<blockquote><i>
	        <li>Some Journal
      	</i></blockquote>
        
        <h3>Conferences</h3>
       	<blockquote><i>
        	<li>Some Conference</li>
       	</i></blockquote>
				
        <?php
        break;
    case 'progcomm':
        ?>
        <h2>Program committee</h2>
       	<blockquote>
        	<li>Some Conference</li>
       	</blockquote>
        <?php
        break;
    case 'techcomm':
        ?>
        <h2>Technical committee</h2>
       	<blockquote>
        	<li>Some Conference</li>
       	</blockquote>

        <?php
        break;
    case 'orgcomm':
        ?>
        <h2>Organizing committee</h2>
       	<blockquote>
        	<li>Some Conference</li>
       	</blockquote>
        <?php
        break;
    case 'membership':
        ?>
        <h2>Membership</h2>
       	<blockquote>
            <li>Some membership</li>
       	</blockquote>
        <?php
        break;
    default:
        include('home-detail.php');
        break;
}
?>