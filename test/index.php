﻿<?php
$naslov = "G P";
$osebaID = 6;
$potDoKorena = "../";
$potDoLokalnegaKorena = "/";
$potDoNavigacije = "";
$prirejenaNoga = "";
$description = "";
$keywords = "";
$robots = "";

$show = '';
if (isset($_GET['show'])) {
    $show = $_GET['show'];
}
?>

<!DOCTYPE html>
<html lang="en">
<?php require($potDoKorena . '_header.php'); ?>

<body>
<?php
  include($potDoKorena . 'header.php'); 
  include ($potDoKorena . '_navigation.php');
?>
<main>
    <div class="container">
    <div class="wrapper">
    <div class="sidebar">
        <h1><?php echo $naslov;?></h1>
        <?php
        include('_navigation.php');
        ?>
    </div>

    <div class="content">
        <?php
        switch ($show) {
            case 'activities':
                include('activities.php');
                break;

            case 'publications':
                include('publications.php');
                break;

            case 'projects':
                include('projects.php');
                break;

            case 'software':
                include('software.php');
                break;
                
            case 'book':
            case 'book-AEAR':
                include('book-AEAR.php');
    						break;

            case 'home':
            default:
                include('home.php');
                break;
        }
        ?>
    </div>

    </div>
    </div>
</main>

<?php
include($potDoKorena . '_footer.php');
?>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>