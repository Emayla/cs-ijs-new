﻿<?php

function setTitle($pressed, $title) {
    $detail = '';
    if (isset($_GET['detail'])) {
        $detail = $_GET['detail'];
    }
    if ($detail == $pressed)
        echo "<span id='obarvaj'>" . $title . "</span>";
    else
        echo $title;
}
?>

<h3>Academic</h3>
<span class='readmore'>
    <a href='?show=activities&detail=mentorship'><?php setTitle('mentorship', 'Mentorship') ?></a><br>
    <a href='?show=activities&detail=courses'><?php setTitle('courses', 'Courses') ?></a><br>
</span>
<p>&nbsp;</p>

<h3>Research</h3>
<span class='readmore'>
    <a href='?show=activities&detail=reviewer'><?php setTitle('reviewer', 'Reviewer') ?></a><br>
    <a href='?show=activities&detail=progcomm'><?php setTitle('progcomm', 'Program committee') ?></a><br>
    <a href='?show=activities&detail=techcomm'><?php setTitle('techcomm', 'Technical committee') ?></a><br>
    <a href='?show=activities&detail=orgcomm'><?php setTitle('orgcomm', 'Organizing committee') ?></a><br>
    <a href='?show=activities&detail=membership'><?php setTitle('membership', 'Membership') ?></a><br>
</span>