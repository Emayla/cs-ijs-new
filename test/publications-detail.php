<?php
if ($_GET['id'] != '') {
    include('../config.php');
    
    $id = $_GET['id'];
    $result = mysql_query("SELECT * FROM publikacije WHERE pubID=$id");
    
    while ($row = mysql_fetch_array($result)) {
        switch ($row[tip]) {
            case 'journal': $i = 1;
                break;
            case 'conference': $i = 2;
                break;
            case 'book': $i = 3;
                break;
            case 'misc': $i = 4;
                break;
            default: $i = 0;
        }
        echo "<h2>$row[naslov]</h2>\n";
        if ($row['avtor1'] != NULL) {
            echo "$row[avtor1]";
            if ($row['avtor2'] != NULL)
                echo ", $row[avtor2]";
            if ($row['avtor3'] != NULL)
                echo ", $row[avtor3]";
            echo "<br>";
        }
        if ($i == 1 && $row['knjiga'] != NULL)
            echo "<i>$row[knjiga]</i>, $row[leto]";
        else if ($i == 2 && $row['knjiga'] != NULL)
            echo "<i>Proc. $row[knjiga]</i>";
        else if ($i == 3 && $row['knjiga'] != NULL)
            echo "$row[knjiga]";
        if ($i == 1) {
            if ($row[letnik] != NULL)
                echo ", Vol. $row[letnik]";
            if ($row[stevilka] != NULL)
                echo ", No. $row[stevilka]";
            if ($row[strani] != NULL)
                echo ", pages: $row[strani]";
            echo "<br />";
        }
        if ($i == 2 || $i == 3) {
            if ($row[urednik1] != NULL) {
                echo ", $row[urednik1]";
                if ($row[urednik2] != NULL)
                    echo ", $row[urednik2]";
                if ($row[urednik3] != NULL)
                    echo ", $row[urednik3]";
                if ($row[urednik2] == NULL && $row[urednik3] == NULL)
                    echo " (Ed.)";
                else
                    echo " (Eds.)";
                echo "<br>";
            }
        }
        if ($i == 2) {
            echo "$row[kraj]: $row[datum] $row[leto]<br />";
            if ($row[strani] != NULL)
                echo "pages: $row[strani]<br>";
        }
        if ($i == 3) {
            echo "$row[zalozba], $row[leto]<br>";
            if ($row[strani] != NULL)
                echo "pages: $row[strani]<br>";
        }
        if ($row[doi] != NULL)
            echo "DOI: <a href='https://dx.doi.org/$row[doi]' target='_new'>$row[doi]</a><br>";
        if ($row[opomba] != NULL)
            echo "note: $row[opomba]<br>";
        if ($row[vir] != NULL)
            echo "<br><a href=\"mailto:$row[vir]?SUBJECT=Request: %22$row[naslov]%22&amp;BODY=Please send me the following paper:%0A$row[naslov]%0A$row[knjiga], $row[leto]%0A%0AWith regards,%0A[your name]\"><b>request for paper</b></a><br>";
        if ($row[bibtex] != NULL)
            echo "<br><i>BibTex:</i><br>$row[bibtex]<br>";
        if ($row[povzetek] != NULL)
            echo "<br><i>abstract:</i><br />$row[povzetek]";
    }
    
    mysql_close($con);
} else {
    include('home-detail.php');
}
?>