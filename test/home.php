		<h3>Born</h3>
		<i>DATE</i><br>
		<a href='http://www.ljubljana.si' TARGET=_top>Ljubljana</a>, <a href='http://www.slovenia.info' TARGET=_top>Slovenia</a><br>
		
		<p>&nbsp;<br>
		<h3>Education</h3>
		<i><a href='papers/BSc.pdf'>B.Sc.</a> (DATE)</i><br>
		<i><a href='papers/MSc.pdf'>M.Sc.</a> (DATE)</i><br>
		<i><a href='papers/PhD.pdf'>Ph.D.</a> (DATE)</i><br>
		<a href='http://www.fe.uni-lj.si' TARGET=_top>Faculty of Electrical Engineering</a>, 
				<a href='http://www.uni-lj.si' TARGET=_top>University of Ljubljana</a><br>
		
		<p>&nbsp;<br>
		<h3>Employment</h3>
		<i>researcher (since DATE)</i><br>
		<a href='http://cs.ijs.si/' TARGET=_blank>Computer Systems</a> at 
				<a href='http://www.ijs.si' TARGET='_blank'>Jožef Stefan Institute</a><br>
				&nbsp;<br>

    
		<p>&nbsp;<br>
		<h3>CV</h3>
    <a href='CV-Europass-NAME.pdf'>CV-Europass</a>

		
		<p>&nbsp;<br>
		<h3>Hobby</h3>
		<i>SOMETHING</i><br>
