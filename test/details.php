<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script>
  function SaveScrollXY() {
    document.Form1.ScrollX.value = document.body.scrollLeft;
    document.Form1.ScrollY.value = document.body.scrollTop;
  }
  function ResetScrollPosition() {
    var hidx, hidy;
    hidx = document.Form1.ScrollX;
    hidy = document.Form1.ScrollY;
    if (typeof hidx != 'undefined' && typeof hidy != 'undefined') {
      window.scrollTo(hidx.value, hidy.value);
      Echo "aaa";
    }
  }
</script>

</head>
<body>
<!--<body onload="ResetScrollPosition()">-->
<div align="left"><br />
<table>
	<tr>
		<td class="main">
			<?php
			include('../config.php');
			
			$dresult = mysql_query("SELECT * FROM publikacije WHERE pubID = ".stripslashes($_GET['id']).' LIMIT 1');
						
			switch ($_GET['type'])
			{
			case journal:
				while($row = mysql_fetch_array($dresult))
				{
					//echo "<img src='../images/bulletred.gif' width='12' height='5' /> <span class='orngheading'>$row[naslov]</span>";
					if($row[avtor1]!=NULL){
					  echo "<ul><li>$row[avtor1]";
						if($row[avtor2]!=NULL)echo ", $row[avtor2]";
						if($row[avtor3]!=NULL)echo ", $row[avtor3]";
						echo "</ul></li>";
					}
					echo "<ul><li><i>$row[knjiga]</i>, $row[leto]";
					if($row[letnik]!=NULL)echo ", Vol. $row[letnik]";
					if($row[stevilka]!=NULL)echo ", No. $row[stevilka]";
					if($row[strani]!=NULL)echo "<br>pages: $row[strani]";
					if($row[opomba]!=NULL)echo "<br>note: $row[opomba]";
					echo "</ul></li>";
					
					if($row[vir]!=NULL)echo "<ul><li><a href=\"mailto:gregor.papa@ijs.si?SUBJECT=Request: %22$row[naslov]%22&amp;BODY=Please send me the following paper:%0A$row[naslov]%0A$row[knjiga], $row[leto]%0A%0AWith regards,%0A[your name]\"><font color=#000000>request for paper</font></a></ul></li>";
					if($row[bibtex]!=NULL)echo "<ul><li><i>BibTex:</i><br>$row[bibtex]</ul></li>";
					if($row[povzetek]!=NULL) echo "<ul><li><i>abstract:</i><br />$row[povzetek]</ul></li>";
				}
				break;
			case conference:
				while($row = mysql_fetch_array($dresult))
				{
					//echo "<img src='../images/bulletred.gif' width='12' height='5' /> <span class='orngheading'>$row[naslov]</span>";
					if($row[avtor1]!=NULL){
					  echo "<ul><li>$row[avtor1]";
						if($row[avtor2]!=NULL)echo ", $row[avtor2]";
						if($row[avtor3]!=NULL)echo ", $row[avtor3]";
						echo "</ul></li>";
					}
					if($row[knjiga]!=NULL) echo "<ul><li><i>Proc. $row[knjiga]</i></ul></li>";
					if($row[urednik1]!=NULL){
						echo "<ul><li>$row[urednik1]";
						if($row[urednik2]!=NULL)echo ", $row[urednik2]";
						if($row[urednik3]!=NULL)echo ", $row[urednik3]";
						if($row[urednik2]==NULL && $row[urednik2]==NULL) echo " (Ed.)</ul></li>";
						else echo " (Eds.)</ul></li>";
					}
					echo "<ul><li>$row[kraj]: $row[datum] $row[leto]";
					if($row[strani]!=NULL)echo "<br /> pages: $row[strani]";
					if($row[opomba]!=NULL)echo "<br /> note: $row[opomba]";
					echo "</ul></li>";
					if($row[vir]!=NULL)echo "<ul><li><a href=\"mailto:gregor.papa@ijs.si?SUBJECT=Request: %22$row[naslov]%22&amp;BODY=Please send me the following paper:%0A$row[naslov]%0A$row[knjiga], $row[leto]%0A%0AWith regards,%0A[your name]\"><font color=#000000>request for paper</font></a></ul></li>";
					if($row[bibtex]!=NULL)echo "<ul><li><i>BibTex:</i><br>$row[bibtex]</ul></li>";
					if($row[povzetek]!=NULL) echo "<ul><li><i>abstract:</i><br />$row[povzetek]</ul></li>";
				}
				break;
			case book:
				while($row = mysql_fetch_array($dresult))
				{
					//echo "<img src='../images/bulletred.gif' width='12' height='5' /> <span class='orngheading'>$row[naslov]</span>";
					if($row[avtor1]!=NULL){
					  echo "<ul><li>$row[avtor1]";
						if($row[avtor2]!=NULL)echo ", $row[avtor2]";
						if($row[avtor3]!=NULL)echo ", $row[avtor3]";
						echo "</ul></li>";
					}
					if($row[knjiga]!=NULL) echo "<ul><li>$row[knjiga]</ul></li>";
					if($row[urednik1]!=NULL){
						echo "<ul><li>$row[urednik1]";
						if($row[urednik2]!=NULL)echo ", $row[urednik2]";
						if($row[urednik3]!=NULL)echo ", $row[urednik3]";
						if($row[urednik2]==NULL && $row[urednik2]==NULL) echo " (Ed.)</ul></li>";
						else echo " (Eds.)</ul></li>";
					}
					echo "<ul><li>$row[zalozba], $row[leto]";
					if($row[strani]!=NULL)echo "<br /> pages: $row[strani]";
					if($row[opomba]!=NULL)echo "<br /> note: $row[opomba]";
					echo "</ul></li>";
					if($row[vir]!=NULL)echo "<ul><li><a href=\"mailto:gregor.papa@ijs.si?SUBJECT=Request: %22$row[naslov]%22&amp;BODY=Please send me the following paper:%0A$row[naslov]%0A$row[knjiga], $row[leto]%0A%0AWith regards,%0A[your name]\"><font color=#000000>request for paper</font></a></ul></li>";
					if($row[bibtex]!=NULL)echo "<ul><li><i>BibTex:</i><br>$row[bibtex]</ul></li>";
					if($row[povzetek]!=NULL) echo "<ul><li><i>abstract:</i><br />$row[povzetek]</ul></li>";
				}
				break;
				case misc:
				while($row = mysql_fetch_array($dresult))
				{
					//echo "<img src='../images/bulletred.gif' width='12' height='5' /> <span class='orngheading'>$row[naslov]</span>";
					if($row[avtor1]!=NULL){
					  echo "<ul><li>$row[avtor1]";
						if($row[avtor2]!=NULL)echo ", $row[avtor2]";
						if($row[avtor3]!=NULL)echo ", $row[avtor3]";
						echo "</ul></li>";
					}
					if($row[knjiga]!=NULL) echo "<ul><li>$row[knjiga]</ul></li>";
					if($row[urednik1]!=NULL){
						echo "<ul><li>$row[urednik1]";
						if($row[urednik2]!=NULL)echo ", $row[urednik2]";
						if($row[urednik3]!=NULL)echo ", $row[urednik3]";
						if($row[urednik2]==NULL && $row[urednik2]==NULL) echo " (Ed.)</ul></li>";
						else echo " (Eds.)</ul></li>";
					}
					echo "<ul><li>$row[zalozba], $row[leto]";
					if($row[strani]!=NULL)echo "<br /> pages: $row[strani]";
					if($row[opomba]!=NULL)echo "<br /> note: $row[opomba]";
					echo "</ul></li>";
					if($row[vir]!=NULL)echo "<ul><li><a href=\"mailto:gregor.papa@ijs.si?SUBJECT=Request: %22$row[naslov]%22&amp;BODY=Please send me the following paper:%0A$row[naslov]%0A$row[knjiga], $row[leto]%0A%0AWith regards,%0A[your name]\"><font color=#000000>request for paper</font></a></ul></li>";
					if($row[bibtex]!=NULL)echo "<ul><li><i>BibTex:</i><br>$row[bibtex]</ul></li>";
					if($row[povzetek]!=NULL) echo "<ul><li><i>abstract:</i><br />$row[povzetek]</ul></li>";
				}
				break;
			default:
				echo "error";
			}
			
			mysql_close();
			?>
		</td>
	</tr>
</table>
</div>
</body>
</html>
