file1 = input("Filename 1: ")
file2 = input("Filename 2: ")

with open(file1, 'r') as f1:
	with open(file2, 'r') as f2:
		i = 0
		for line in f1:
			l1 = line.strip()
			l2 = f2.readline().strip()
			if l1 != l2:
				print("Line: " + i)
				print(file1 + ": " + l1)
				print(file2 + ": " + l2)
				print("-------")
			i += 1