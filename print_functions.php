<?php
	function printPublication($row, $type, $byid) {
    // $byid je boolean; 1 - printamo eno samo publikacijo, namesto lijev in linka bo header
    echo $byid ? "" : "<li>";
    echo $byid ? "" : "<a name='$row[pubID]-header' class='anchor'> \n";
    echo $byid ? "<h1>" : "<a href='#" . $row['pubID'] . "-header' onclick='toggleAbbrDesc(this, \"" . $row['pubID'] . "\");'>";
    echo $row['naslov'];
    echo $byid ? "</h1> \n" : "</a> \n";
    if (!$byid) {
      echo "<abbr id='abbr-" . $row['pubID'] . "'>";
      if ($type == 'books') {
        echo ($row['knjiga'] != '' ? ", <em>" . $row['knjiga'] . "</em>" : "") . ", $row[zalozba] ($row[leto])";
      }
      else echo ", <em>$row[akronim]</em>";
      echo "</abbr> \n";
      echo "<div class='permalink'> \n";
      echo "<a href='?show=publications&id=$row[pubID]'><span class='glyphicon glyphicon-link' aria-hidden='true'></span></a> \n";
      echo "</div> \n";
      echo "</a>";
      echo "</li> \n";
    }

    echo "<div id='" . $row['pubID'] . "' class='";
    echo $byid ? "publication-id" : "publication";
    echo "'> \n";
    
    // avtorji
    echo "<p> \n";
    if ($row['avtor1'] != NULL) {
      echo "$row[avtor1]";
      if ($row['avtor2'] != NULL)
          echo ", $row[avtor2]";
      if ($row['avtor3'] != NULL)
          echo ", $row[avtor3]";
      echo "<br> \n";
    }

    if ($type == 'journal') {
    	if ($row['knjiga'] != NULL) echo "<em>$row[knjiga]</em>, $row[leto]";
    	if ($row['letnik'] != NULL) echo ", Vol. $row[letnik]";
    	if ($row['stevilka'] != NULL) echo ", No. $row[stevilka]";
    	if ($row['strani'] != NULL) echo ", pages: $row[strani]";
    	echo "</p> \n";
    }
    else {  // conference papers or books
    	if ($row['knjiga'] != NULL) {
    		if ($type == 'conference') echo "<em>Proc. $row[knjiga]</em>";
    		elseif ($type == 'books') echo "$row[knjiga]";
    	}
    	if ($row['urednik1'] != NULL) {
		    echo ", $row[urednik1]";
		    if ($row['urednik2'] != NULL) echo ", $row[urednik2]";
		    if ($row['urednik3'] != NULL) echo ", $row[urednik3]";

		    if ($row['urednik2'] == NULL && $row['urednik3'] == NULL) echo " (Ed.)";
	    	else echo " (Eds.)";
    	}
	    echo "<br> \n";

    	if ($type == 'conference') echo "$row[kraj]: $row[datum] $row[leto] \n";
	    elseif ($type == 'books') echo "$row[zalozba], $row[leto] \n";

		  if ($row['strani'] != NULL) echo "<br>pages: $row[strani]</p> \n";
	    else echo "</p> \n";
    }

    if ($row['opomba'] != NULL) echo "<p>note: $row[opomba] </p> \n";
    if ($row['vir'] != NULL) echo "<p><a href=\"mailto:$row[vir]?SUBJECT=Request: %22$row[naslov]%22&amp;BODY=Please send me the following paper:%0A$row[naslov]%0A$row[knjiga], $row[leto]%0A%0AWith regards,%0A[your name]\"><b>request for paper</b></a></p> \n";
    if ($row['bibtex'] != NULL) echo "<p><em>BibTex:</em><br>$row[bibtex]</p> \n";

    if ($row['doi'] != NULL) {
	    echo "<p>";
	   	echo "DOI: <a href='https://dx.doi.org/$row[doi]' target='_blank'>$row[doi]</a>";
	    echo "</p> \n";	
    }
   
    if ($row['povzetek'] != NULL) {
      echo "<h3>abstract</h3> \n";
      echo "<p>$row[povzetek]</p> \n";
    }
    echo "</div> \n\n";
	}


	function printPublications($type, $result, $all) {
		$today = getdate();
		$trenutnoLeto = $today['year'];
	  $trenutnoLetoPrvic = true;  // naslov s trenutnim letom se sprinta le, ce obstaja kaksna publikacija v tem letu

	  while ($row = mysql_fetch_array($result)) {
	    if (($trenutnoLetoPrvic && $trenutnoLeto == $row['leto']) || ($trenutnoLeto != $row['leto'])) {
	      $trenutnoLetoPrvic = false;
	      $trenutnoLeto = $row['leto'];
	      echo "<h2>" . $trenutnoLeto . "</h2> \n";
	    }
	    printPublication($row, $type, 0);
	  }
	}


  function printProject($row, $byid) {
  	// $byid je boolean; 1 - printamo en sam projekt, namesto lijev in linka bo header

    if ($row['splet'] != NULL || $row['opis'] != NULL) {
      echo $byid ? "" : "<li>";
      echo $byid ? "" : "<a name='$row[projID]-header' class='anchor'> \n";
      echo $byid ? "<h1>" : "<a href='#" . $row['projID'] . "-header' onclick='toggleAbbrDesc(this, \"" . $row['projID'] . "\");'>"; 
      echo $row['naslov']; 
      echo $byid ? "</h1> \n" : "</a> \n";
      if (!$byid) {
      	echo "<abbr id='abbr-" . $row['projID'] . "'>";
	      if ($row['akronim'] != '') echo " - $row[akronim]";
	      echo " (" . $row['leto_zacetek'] . "-" . $row['leto_konec'] . ")";
	      echo "</abbr> \n";
	      echo "<div class='permalink'> \n";
      	echo "<a href='?show=projects&id=$row[projID]'><span class='glyphicon glyphicon-link' aria-hidden='true'></span></a> \n";
      	echo "</div> \n";
	      echo "</a>";
	      echo "</li> \n";	
      }

      echo "<div id='" . $row['projID'] . "' class='";
      echo $byid ? "publication-id" : "publication";
      echo "'> \n";

      echo "<p> \n";
      if ($row['akronim'] != NULL) echo "acronym: $row[akronim]<br> \n";
      if ($row['vrsta'] != NULL) echo "type: $row[vrsta]<br> \n";

      if(($row['tip'] == "application" || $row['tip'] == "international") && $row['ustanova'] != NULL) {
        echo "contractor: $row[ustanova]";
        if($row['mesto'] != NULL) echo ", $row[mesto]";
        if($row['drzava'] != NULL) echo ", $row[drzava]";
        echo "<br> \n";
      }

      echo "duration: $row[leto_zacetek]";
      if ($row['leto_zacetek'] != $row['leto_konec']) echo "-$row[leto_konec]";
      echo "\n<br>";
      
      if ($row['splet'] != NULL) echo " \n<a href='$row[splet]' target='_blank'>$row[splet]</a>";
      echo "\n</p> \n";

      if ($row['opis'] != NULL) {
        echo "<h3>content:</h3> \n";
        echo "<p>$row[opis]</p> \n";
      }
      echo "</div> \n\n";
    }
    else {
    	echo "<li> \n";
    	echo $row['naslov'];
    	echo "<abbr id='abbr-" . $row['projID'] . "'>";
		  if ($row['akronim'] != '') echo " - $row[akronim]";
		  echo " (" . $row['leto_zacetek'] . "-" . $row['leto_konec'] . ")";
		  echo "</abbr> \n";
    	echo "</li> \n";
    }
  }


  function printProjects($detail) {
    include('config.php');
    $today = getdate();

    $all = 0;

    if (isset($_GET[$detail])) {
      if ($_GET[$detail] == 'all') $all = 1;
    }

    echo "<ul class='publications'> \n";

    $leto = $today['year'];
    $meja = $all ? 0 : $leto - 6;
    
    if ($detail == 'research') {
      $result = mysql_query("SELECT * FROM projekti WHERE (prikaz='1' AND ((tip='program' 
        AND leto_konec > ($meja+2)) OR (tip='research' AND leto_konec > $meja) 
        OR (tip='international' AND leto_konec > $meja))) ORDER BY leto_zacetek DESC, leto_konec DESC");  
    }
    else {  // $detail == applied
        $result = mysql_query("SELECT * FROM projekti WHERE (prikaz='1' AND tip='application' 
          AND leto_konec > $meja) ORDER BY leto_zacetek DESC, leto_konec DESC");
    }

    
    while ($row = mysql_fetch_array($result)) {
      printProject($row, 0);
    }

    mysql_close($con);
  }

  function printID($show, $id) {
    include('config.php');
    $id = mysql_escape_string($id);
    if ($show == 'publications') $result = mysql_query("SELECT * FROM publikacije WHERE (pubID='$id')");
    elseif ($show == 'projects') $result = mysql_query("SELECT * FROM projekti WHERE (projID='$id')");

    while ($row = mysql_fetch_array($result)) {
      $type = $row['tip'];
      if ($type == 'book') $type = 'books';

      if ($show == 'publications') printPublication($row, $type, 1);
      elseif ($show == 'projects') printProject($row, 1);
    }

    mysql_close($con);
  }
?>