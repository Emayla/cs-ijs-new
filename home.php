<script src="js/toggle.js"></script>
<main>
  <div class="container">
  <div id="welcome">
    <div id="spotlight">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="images/banners/banner06.png" alt="">
          </div>
          <div class="item">
            <img src="images/banners/banner07.png" alt="">
          </div>
          <div class="item">
            <img src="images/banners/banner08.png" alt="">
          </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
      </div>
    </div>

    <?php include('about.php'); ?>
  </div>

  <div id="our-activities">

    <!-- News and events -->
    <div id="news">
      <h2><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> News and events 
      <small><a href="index.php?show=news">show more</a></small></h2>

      <?php
        include('news-func.php');
        printNews(true, false);
      ?>
      
    </div> 

    <div id="contact">
      <h2><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> Contacts</h2>
      <div id="address">
        <p>Computer Systems <br>
        Jožef Stefan Institute <br>
        Jamova cesta 39 <br>
        SI-1000 Ljubljana <br>
        Slovenia</p>
      </div>
      <div id="phone">
        <p>+386 1 477 3514 <em>(Head of Department)</em> <br>
        +386 1 477 3582 <em>(Secretary)</em> <br>
        +386 1 477 3882 <em>(fax)</em> <br>
        <a href="mailto:cs@ijs.si">cs@ijs.si</a></p>
      </div>
    </div>
  </div>
  
  </div>
</main>