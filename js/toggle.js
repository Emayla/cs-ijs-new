$(document).ready(function(){
	$(".toggle").on("click", function() {
	  var el = $(this);
	  el.find('span').toggleClass('glyphicon-plus').toggleClass('glyphicon-minus');
	});
});

function toggleAbbrDesc(el, id) {
	var weight = $(el).css('font-weight');
	if (weight == '400' || weight == 'normal')
		$(el).css({'font-weight': '500'});
	else if (weight == '500')
		$(el).css({'font-weight': '400'});

	$("#" + id).toggle('fast');
  $("#abbr-" + id).toggle();
}