$(document).ready(function () {
  var menu = $('.navbar');
  var origOffsetY = menu.offset().top;

  function scroll() {
    if ($(window).scrollTop() >= origOffsetY) {
        $('.navbar').addClass('navbar-fixed-top');
        $('main').addClass('menu-padding');
    } else {
        $('.navbar').removeClass('navbar-fixed-top');
        $('main').removeClass('menu-padding');
    }
  }

  document.onscroll = scroll;
});