<main>
	<div class="container">
	<div class="wrapper">
	<div class="sidebar">
		<h1>Staff</h1>
		<ul>
			<li class='<?php echo ($detail == "" || $detail == "staff-members" ? "active" : "") ?>'><a href="?show=staff&detail=staff-members">Staff</a></li>
			<li class='<?php echo ($detail == "associates" ? "active" : "") ?>'><a href="?show=staff&detail=associates">Associates</a></li>
		</ul>
	</div>

	<div class="content">
		<?php
			switch ($detail) {
				case 'staff-members':
				case '':
					echo "<h1>Staff</h1> \n";
					break;
				case 'associates':
					echo "<h1>Associates</h1> \n";
					break;
				default:
					include('404.php');
					break;
			}

			$filename_detail = $detail . '.php';
			if (is_file($filename_detail)) {
				include($filename_detail);
			}
			elseif ($detail == '') {
				include('staff-members.php');
			}
		?>
	</div>
	</div>
	</div>
</main>