<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span> Menu
      </button>
      <div id="navbar-logo">
        <a class="navbar-brand" href="<?php echo $potDoKorena; ?>index.php"><img src="<?php echo $potDoKorena; ?>images/logos/E7-logo.png"></a>
      </div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-menu">
      <ul class="nav navbar-nav">
        <li class='<?php echo ($show == "" || $show == "home" ? "active" : "")?>'><a href="<?php echo $potDoKorena; ?>?show=home">Home <span class="sr-only">(current)</span></a></li>
        <li class='<?php echo ($show == "activities" ? "active" : "")?>'><a href="<?php echo $potDoKorena; ?>?show=activities">Activities</a></li>
        <li class='<?php echo ($show == "publications" ? "active" : "")?>'><a href="<?php echo $potDoKorena; ?>?show=publications">Publications</a></li>
        <li class='<?php echo ($show == "projects" ? "active" : "")?>'><a href="<?php echo $potDoKorena; ?>?show=projects">Projects</a></li>
        <li class='<?php echo ($show == "staff" ? "active" : "")?>'><a href="<?php echo $potDoKorena; ?>?show=staff">Staff</a></li>

        <?php
        switch($_SERVER['REMOTE_ADDR']){
          case '194.249.231.233'://gregor
          case '194.249.231.222'://peter
          case '194.249.231.162'://peter
          case '194.249.231.184'://jure
          case '194.249.231.230'://drago
            echo "<li><a href='/sporocila'><span class='glyphicon glyphicon-wrench' aria-hidden='true'></span></a></li>";
            break;
          }
        ?>

      </ul>
    </div>
  </div>
</nav>