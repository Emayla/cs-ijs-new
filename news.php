<?php
	$all = 0;

  if (isset($_GET['news'])) {
    if ($_GET['news'] == 'all') $all = 1;
  }
?>

<main>
	<div class="container">
	<div class="wrapper">
	<div class="sidebar">
		<h1>News</h1>
		<ul>
			<li class='<?php echo (!($all) ? "active" : "") ?>'><a href="index.php?show=news">Show recent</a></li>
			<li class='<?php echo ($all ? "active" : "") ?>'><a href="index.php?show=news&news=all">Show all</a></li>
		</ul>
	</div>

	<div class="content">
		<h1>News</h1>

		<?php
			include('news-func.php');
			printNews(false, $all);
		?>

	</div>
	</div>
	</div>
</main>