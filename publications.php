<?php
	require('print_functions.php');
?>

<script src="js/toggle.js"></script>
<main>
	<div class="container">
	<div class="wrapper">
	<div class="sidebar">
		<h1>Publications</h1>
		<ul>
			<li class='<?php echo (($detail == "" && $id == "") || $detail == "journal" ? "active" : "") ?>'><a href="?show=publications&detail=journal">Journal articles</a></li>
			<li class='<?php echo ($detail == "conference" ? "active" : "") ?>'><a href="?show=publications&detail=conference">Conference papers</a></li>
			<li class='<?php echo ($detail == "books" ? "active" : "") ?>'><a href="?show=publications&detail=books">Books & Chapters</a></li>
		</ul>
	</div>

	<div class="content">
		<?php
			if ($id != '') {
				printID($show, $id);
			}
			else {
				switch ($detail) {
					case 'journal':
					case '':
						echo "<h1>Journal articles<small><a href='?show=publications&detail=journal";
						if (isset($_GET[$detail])) {
							if ($_GET[$detail] == 'all') echo "'>show recent";
							else echo "&journal=all'>show all";
						}
						else echo "&journal=all'>show all";
						echo "</a></small></h1> \n";
						break;
					case 'conference':
						echo "<h1>Conference papers<small><a href='?show=publications&detail=conference";
						if (isset($_GET[$detail])) {
							if ($_GET[$detail] == 'all') echo "'>show recent";
							else echo "&conference=all'>show all";
						}
						else echo "&conference=all'>show all";
						echo "</a></small></h1> \n";
						break;
					case 'books':
						echo "<h1>Books & Chapters<small><a href='?show=publications&detail=books";
						if (isset($_GET[$detail])) {
							if ($_GET[$detail] == 'all') echo "'>show recent";
							else echo "&books=all'>show all";
						}
						else echo "&books=all'>show all";
						echo "</a></small></h1> \n";
						break;
					default:
						include('404.php');
						break;
				}

				$filename_detail = $detail . '.php';
				if (is_file($filename_detail)) {
					include($filename_detail);
				}
				elseif ($detail == '') {
					include('journal.php');
				}
			
			}
		?>

	</div>
	</div>
	</div>
</main>