<?php
	function printNews($latest, $all) {
		include('config.php');

		if ($all) {
			$result = mysql_query("SELECT * FROM novice WHERE CURRENT_DATE >= datum_vpis ORDER BY datum_vpis DESC");  // vsi
		}
		else {
			// delujoči za aktualne
			// $result = mysql_query("SELECT * FROM novice WHERE CURRENT_DATE <= datum_konec ORDER BY datum_vpis DESC");
			
			// if (mysql_num_rows($result) == 0) {
				// tako da je vedno vsaj nekaj
			$result = mysql_query("SELECT * FROM novice WHERE (YEAR(CURRENT_DATE)-1 <= YEAR(datum_vpis) AND datum_vpis <= CURRENT_DATE) ORDER BY datum_vpis DESC");
			// }
		}
		
		$datumdanes = mysql_query("CURRENT_DATE");
		$datumdanes = date("Y-m-d");
		$newscount = 0;
		$maxitems = 4;

		while ($row = mysql_fetch_array($result)) {
			$newscount++;
			echo "<div class='";
			echo $latest ? "event" : "news-article";
			echo "'> \n";
			
			$originalDate = $row['datum_vpis'];
			$newDate = date("d. M Y", strtotime($originalDate));
			echo "<div class='title'> \n";
			echo "<span class='date'>" . $newDate . "</span> \n";
			echo "<div class='header'> \n";
			echo $latest ? "<a href='#" . $row['novicaID'] . "' data-toggle='collapse'><div class='toggle'><span class='glyphicon glyphicon-plus gi-smaller' aria-hidden='true'></span> \n" : "";
			echo $latest ? "<h3>" : "<h2>";
			echo $row['naslov'];
			echo $latest ? "</h3></div></a> \n" : "</h2> \n";
		 	echo "</div> \n";  // header
			echo "</div> \n";  // title

			// slika
			$kje = "../images/news/newsID";
			$resultFile = glob($kje . $row['novicaID'] . ".*");
			$ext = end(explode('.', $resultFile[0]));
			$image_path = $kje . $row['novicaID'] . "." . $ext;

			echo "<div id='" . $row['novicaID'] . "' class='";
			echo $latest ? "collapse " : "";
			echo "description'> \n";
			if (file_exists($image_path)) {
				echo "<a href='" . $row['povezava'] . "'><img src='" . $image_path . "' width='100' class='img-thumbnail' alt='" . $row['naslov'] . "'></a> \n";
			}
			echo "$row[opis] \n";
			echo "</div> \n";

			echo "</div> \n"; // event / news-article

			if (($newscount >= $maxitems) && $latest) break;
		}
		
		mysql_close($con);	
	}
?>