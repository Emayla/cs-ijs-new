<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title><?php echo $naslov; ?></title>
  <meta name="description" content="<?php echo $description; ?>" />
  <meta name="keywords" content="<?php echo $keywords; ?>" />
  <meta name="robots" content="<?php echo $robots; ?>">

  <link rel="icon" href="<?php echo $potDoLokalnegaKorena; ?>images/favicon.ico" type="image/x-icon"/>
  <link rel="shortcut icon" href="<?php echo $potDoLokalnegaKorena; ?>images/favicon.ico" type="image/x-icon"/>

  <!-- Bootstrap -->
  <link href="<?php echo $potDoKorena; ?>css/bootstrap.min.css" rel="stylesheet">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  
  <!-- Custom style & font -->
  <link href="<?php echo $potDoKorena; ?>css/style.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

  <script src="<?php echo $potDoKorena; ?>js/menu.js"></script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  [endif] -->
</head>