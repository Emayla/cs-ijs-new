<div id="conferences" class="collapse in">
  <ul>
    <li><a href="http://www.hci.si/hci-2016/" target="_blank">HCI-IS 2016</a> - Human-Computer Interaction in Information Society</li>
    <li><a href='http://bioma.ijs.si/conference/2016/' target=_blank>BIOMA 2016</a> - The 7th International Conference on Bioinspired Optimization Methods and their Applications</li>
    <li><a href='http://www.hci.si/is/' target=_blank>HCI-IS 2014</a> - Human-Computer Interaction in Information Society</li>
    <li><a href='http://ppsn2014.ijs.si/' target=_blank>PPSN 2014</a> - The 13th International Conference on Parallel Problem Solving From Nature</li>
    <li><a href='http://bioma.ijs.si/conference/2014/' target=_blank>BIOMA 2014</a> - The Student Workshop on Bioinspired Optimization Methods and their Applications</li>
    <li><a href='http://www.hci.si/is/' target=_blank>HCI-IS 2013</a> - Human-Computer Interaction in Information Society</li>
    <li><a href='http://bioma.ijs.si/conference/2012/' target=_blank>BIOMA 2012</a> - The 5th International Conference on Bioinspired Optimization Methods and their Applications</li>
    <li><a href='http://bioma.ijs.si/conference/2010/' target=_blank>BIOMA 2010</a> - The 4th International Conference on Bioinspired Optimization Methods and their Applications</li>
    <li><a href='http://bioma.ijs.si/conference/2008/' target=_blank>BIOMA 2008</a> - The 3rd International Conference on Bioinspired Optimization Methods and their Applications</li>
    <li><a href='http://www.midem-drustvo.si/conf2007/' target=_blank>MIDEM 2007 Workshop on Electronic Testing</a></li>
    <li><a href='http://bioma.ijs.si/conference/2006/' target=_blank>BIOMA 2006</a> - The 2nd International Conference on Bioinspired Optimization Methods and their Applications</li>
    <li><a href='http://bioma.ijs.si/conference/2004/' target=_blank>BIOMA 2004</a> - International Conference on Bioinspired Optimization Methods and their Applications</li>
  </ul>
</div>