<?php 
	function generatePerson($ime, $priimek, $imgPath, $soba, $telefon, $email, $url='', $opis='', $funkcija='', $fax='') {
		echo "<div class='person-name'><h2>" . $ime . " " . $priimek . " <small>" . $funkcija . "</small></h2></div> \n";
		echo "<div class='person'> \n";

		// Slika
		echo "<div class='image'>";
		if (is_file($imgPath)) {
			echo "<img src=" . $imgPath . 
			" class='img-responsive img-thumbnail center-block' alt='" . $ime . " " . $priimek . "'>";
		}
		echo "</div> \n";

		// Podatki
		echo "<div class='description'> \n";
		echo "<p>";
		echo $soba . " <br> \n";
		echo $telefon . " <br> \n";
		if ($fax != '') echo $fax . " (fax) <br> \n";
		echo "<a href='mailto:" . $email . "'>" . $email . "</a> <br> \n";
		if ($url != '') echo "<a href='" . $url . "' target='_blank'>" . $url . "</a> <br> \n";
		echo "</p> \n";
		// Opis
		if ($opis != '') echo "<p>" . $opis . "</p> \n";
		echo "</div> \n";

		echo "</div> \n";
		echo "<hr> \n";
	}


	// Branje osebja iz baze
	include('config.php');
	$result = mysql_query("SELECT * FROM osebje WHERE status=1 ORDER BY priimek ASC");

	while ($row = mysql_fetch_array($result)) {
		// poiscemo pot do slike
		if ($row[osebjeID] == 36) {  // posebnost pri Kovacicu
			$file_name = "../kovacic/images/" . $row[ime];
		} 
		else {
			$sumniki = array("č", "š", "ž", "Č", "Š", "Ž", "ć", "Ć", "đ", "Đ");
			$sicniki = array("c", "s", "z", "C", "S", "Z", "c", "C", "d", "D");
			$folder1 = explode("/", $row[url]);
			$folder = $folder1[3];
			$file = $row[ime];
			$file = str_replace($sumniki, $sicniki, $file);
			$file_name = "../" . $folder . "/images/" . $file;
		}
		
		if (file_exists($file_name . ".png")) 		$ext = "png";
		else if(file_exists($file_name . ".jpg")) $ext = "jpg";
		else if(file_exists($file_name . ".gif")) $ext = "gif";
		else if(file_exists($file_name.".bmp")) 	$ext = "bmp";
		else {
			$file_name = strtolower($file_name);
			if(file_exists($file_name.".png")) 			$ext="png";
			else if(file_exists($file_name.".jpg")) $ext="jpg";
			else if(file_exists($file_name.".gif"))	$ext="gif";
			else if(file_exists($file_name.".bmp"))	$ext="bmp";
		}
		
		$file_name = $file_name . "." . $ext;

		if ($row[showurl] == '1') $url = $row[url];
		else $url = '';

		// sprintamo osebo
		generatePerson($row[ime], $row[priimek], $file_name, $row[soba], $row[telefon], $row[email], $url, 
			$row[opis], $row[funkcija], $row[fax]);

		// if($row[naziv1]!='') echo $row[naziv1]." ";
		// if($row[naziv]!='') echo ", ".$row[naziv];
		// if($row[fax]!='')echo $row[faks]." (fax)<br>\n";
  }
	mysql_close($con);
?>