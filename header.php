<header>
  <div class="container">
    <div id="header-logo">
      <a href="http://ijs.si" target="_blank"><img src="<?php echo $potDoKorena; ?>images/logos/ijs-logo.png" alt="Jožef Stefan Institute"></a>
    </div>
    <div id="header-links">
      <ul class="header-links">
        <li><a href="mailto:cs@ijs.si"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Contact us</a></li>
      </ul>
    </div>
  </div>
</header>